class Admin::AdminController < ApplicationController

  before_filter :authenticate_user!
  before_filter :user_signed_in?
end