Rails.application.routes.draw do

  root "home#index"
  get 'home/index'

  resources :categories
  resources :posts

  #routes by gems
  devise_for :users
    
  namespace :admin do

    root "posts#index" 
    resources :posts
    resources :categories
    resources :users
  end

end
